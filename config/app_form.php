<?php
return [
	'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
	'error' => '<div class="invalid-feedback">{{content}}</div>'
]	
?>