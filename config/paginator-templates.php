<?php
return [
	'number' => '<li class = "page-item"><a href = "{{url}}" class = "page-link">{{text}}</a></li>',
	'current' => '<li class = "page-item active"><a href = "{{url}}" class = "page-link">{{text}}</a></li>',
	'first' => '<li class = "page-item"><a href = "{{url}}" class = "page-link">&laquo;</a></li>',
	'last' => '<li class = "page-item"><a href = "{{url}}" class = "page-link">&raquo;</a></li>',
	'prevActive' => '<li class = "page-item"><a href = "{{url}}" class = "page-link">&lt;</a></li>',
	'nextActive' => '<li class = "page-item"><a href = "{{url}}" class = "page-link">&gt;</a></li>',
];
?>