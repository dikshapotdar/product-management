<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;

class FileComponent extends Component
{
    /**
     * Saves uploaded file uploaded by user to the location given in $path
     *
     * @param string $targetpath path of file where file uploaded by user
     * @param string $file Full path where file must be moved
     * @param \App\Controller\Component\file $oldimage Image which is already stored
     * @return void
     */
    public function uploadFile($targetpath, $file, $oldimage = '')
    {
        if (!empty($oldimage)) {
            $unlink = unlink(TARGET_PATH . $oldimage);
        }
        $move = $file->moveTo($targetpath);
    }

    /**
     * Sets the file name to variable $filename genrating a unique name for a file
     *
     * @param \App\Controller\Component\file $file File to be uploaded by user
     * @return string
     */
    public function genrateFilename($file)
    {
        $filename = $file->getClientFilename();
        $fileExt = '.' . pathinfo($filename, PATHINFO_EXTENSION);
        $newfilename = time() . '-' . rand(1000, 9999) . $fileExt;

        return $newfilename;
    }
}
