<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;

class ProductsController extends AppController
{
    /**
     * Intialize method load models, components which is used in this component methods
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel("Products");
        $this->loadComponent('Paginator');
        $this->loadModel("UsersProducts");
        $this->loadComponent('File');
        $this->Authorization->skipAuthorization();
        $this->paginate = [
            'limit' => '3',
        ];
    }

    /**
     * Display Table view of all products
     *
     * @return \App\Controller\Cake\Http\Response
     */
    public function index()
    {
        try {
            $product = $this->paginate($this->Products->find('all'));
            $products = $this->Products->newEmptyEntity();
            if ($this->Authorization->can($products, 'index')) {
                $this->set('products', $product);
            } else {
                $this->Flash->error('You are not authorized for this action!');

                return $this->redirect(['_name' => 'home']);
            }
        } catch (NotFoundException $e) {
            return $this->redirect(['_name' => 'listofproducts']);
        }
    }

    /**
     * Add new products
     *
     * @return \App\Controller\Cake\Http\Response
     */
    public function add()
    {
        $product = $this->Products->newEmptyEntity();
        if ($this->Authorization->can($product, 'add')) {
            if ($this->request->is('post', 'put')) {
                $product = $this->Products->patchEntity(
                    $product,
                    $this->request->getData(),
                    ['validate' => 'Add']
                );
                $file = $this->getRequest()->getData('image');
                $filename = $this->File->genrateFilename($file);
                $product->img = $filename;
                $this->set('products', $product);
                if ($this->Products->save($product)) {
                    $targetpath = TARGET_PATH . $filename;
                    $this->File->uploadFile($targetpath, $file, "");
                    $this->Flash->success('Product Successfully Added !');

                    return $this->redirect(['_name' => 'listofproducts']);
                }
            }
            $this->set(compact('product'));
        } else {
            $this->Flash->error('You are not authorized for this action!');

            return $this->redirect(['_name' => 'home']);
        }
    }

    /**
     * Edit products by id
     *
     * @param int $id by id edit the particular product
     * @return \App\Controller\Cake\Http\Response
     */
    public function edit($id)
    {
        $product = $this->Products->get($id);
        if ($this->Authorization->can($product, 'edit')) {
            if ($this->request->is(['post', 'put'])) {
                $product = $this->Products->patchEntity(
                    $product,
                    $this->request->getData(),
                    ['validate' => 'Edit']
                );
                $oldimage = "";
                $file = $this->getRequest()->getData('image');
                $filename = $file->getClientFilename();
                $oldimage = $product->img;
                if (!empty($filename)) {
                    $filename = $this->File->genrateFilename($file);
                    $product->img = $filename;
                    $targetpath = TARGET_PATH . $filename;
                }
                if ($this->Products->save($product)) {
                    if (!empty($filename)) {
                        $this->File->uploadFile($targetpath, $file, $oldimage);
                    }
                    $this->Flash->success('Product Successfully Updated !');

                    return $this->redirect(['_name' => 'listofproducts']);
                }
            }
            $this->set(compact('product'));
        } else {
            $this->Flash->error('You are not authorized for this action!');

            return $this->redirect(['_name' => 'home']);
        }
    }

    /**
     * Delete products by id
     *
     * @param int $id by id delete the particular product
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function delete($id)
    {
        $product = $this->Products->get($id);
        if ($this->request->is('ajax')) {
            $oldimage = $product->img;
            if ($this->Products->delete($product)) {
                unlink(TARGET_PATH . $oldimage);
                $response = json_encode([
                    'status' => 'success', 'message' => 'Delete Product Successfully!',
                    'data' => ['alert-class' => 'alert-success'],
                ]);
                $this->response = $this->response->withStringBody($response);

                return $this->response;
            }
        }
    }

    /**
     * Table view of all add to cart products of users
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function allcart()
    {
        try {
            $product = $this->paginate($this->Products->allProduct());
            $this->set('products', $product);
        } catch (NotFoundException $e) {
            return $this->redirect(['action' => 'allcart']);
        }
    }
}
