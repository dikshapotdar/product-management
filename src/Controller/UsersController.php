<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;
use Cake\Utility\Security;

class UsersController extends AppController
{
    use MailerAwareTrait;

    /**
     * Intialize method load models, components which is used in this component methods
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('loginLayout');
        $this->loadModel("Users");
        $this->Authorization->skipAuthorization();
    }

    /**
     * Before filter method run after intialize method in this method allows the allowUnauthenticated views
     *
     * @param \Cake\Event\EventInterface $event gives event to before filter method
     * @return void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['login',
            'register',
            'forgotpassword',
            'resetpassword',
            'verification',
        ]);
    }

    /**
     * Register new user
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function register()
    {
        $userdata = $this->Users->newEmptyEntity();
        if ($this->request->is('post', 'put')) {
            $userdata = $this->Users->patchEntity(
                $userdata,
                $this->request->getData(),
                ['validate' => 'register']
            );

                $token = Security::hash(Security::randomBytes(25));
                $reset_token_link = Router::url(['_name' => 'verification'], true) . '/' . $token;
                $emaildata = [$userdata->email, $reset_token_link];
                $userdata->token = $token;
            if ($this->Users->save($userdata)) {
                $this->getMailer('User')->send('verification', [$emaildata]);
                $this->Flash->success('User Succesfully Register, Confirmation mail sent to your email!');

                return $this->redirect(['_name' => 'login']);
            }
        }
        $this->set('users', $userdata);
    }

    /**
     * Verify the new user email
     *
     * @param string $token which is set at the time of registration
     * @return void
     */
    public function verification($token = null)
    {
        $user = $this->Users->verifyToken($token);
        if ($user) {
            $user->token = null;
            $user->verify = 1;
            $this->Users->save($user);
            $this->set(compact('user'));
        }
    }

    /**
     * User Forgot the password
     *
     * @return void
     */
    public function forgotpassword()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->getData())) {
                $get = $this->request->getData();
                $email = $get['email'];
                $user = $this->Users->findByEmail($email)->first();

                if (!empty($user)) {
                    $token = Security::hash(Security::randomBytes(25));
                    $reset_token_link = Router::url(['_name' => 'resetpassword'], true) . '/' . $token;
                    $emaildata = [$user->email, $reset_token_link];
                    $user->token = $token;
                    if ($this->Users->save($user)) {
                        $this->getMailer('User')->send('forgotpassword', [$emaildata]);
                        $this->Flash
                        ->success('Please click on password reset link, sent in your email address to reset password.');
                    }
                } else {
                    $this->Flash->error('Sorry! Email address is not available here.');
                }
            }
        }
    }

    /**
     * Reset the users password
     *
     * @param string $token which is set at the time of forgotpassword
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function resetpassword($token = null)
    {
        $user = $this->Users->verifyToken($token);
        if ($user) {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $user = $this->Users->patchEntity($user, $this->request->getData(), [
                    'validate' => 'resetpassword',
                ]);
                $newPassword = $this->getRequest()->getData('newPassword');
                $user->token = null;
                $user->password = $newPassword;
                if ($this->Users->save($user)) {
                    $this->Flash->success('Reset Password Succesfully!');

                    return $this->redirect(['_name' => 'login']);
                }
            }
            $this->set(compact('user'));
        } else {
            $this->Flash->error('Invalid!');

            return $this->redirect(['_name' => 'login']);
        }
    }

    /**
     * Login the user by using email and password
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function login()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $identity = $this->Authentication->getIdentity();
            $role = $identity->get('role');
            $name = $identity->get('name');
            $verify = $identity->get('verify');
            if ($verify == '1') {
                if ($role == 'admin') {
                    $this->Flash->login('Welcome ' . $name . '...');
                    $target = $this->Authentication->getLoginRedirect() ?? '/products/index';

                    return $this->redirect($target);
                } else {
                    $this->Flash->login('Welcome ' . $name . '...');
                    $target = $this->Authentication->getLoginRedirect() ?? '/usersproducts/index';

                    return $this->redirect($target);
                }
            } else {
                $this->Authentication->logout();
                $this->Flash->error('Please verify your email !');

                return $this->redirect(['_name' => 'login']);
            }
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error('Invalid username or password');
        }
    }

    /**
     * Logout the user.
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function logout()
    {
        $this->Authentication->logout();

        return $this->redirect(['_name' => 'login']);
    }
}
