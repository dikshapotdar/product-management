<?php
declare(strict_types=1);

namespace App\Controller;

class UsersProductsController extends AppController
{
    /**
     * Intialize method load models, components which is used in the methods
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel("Products");
        $this->loadComponent('Paginator');
        $this->loadModel("UsersProducts");
        $this->viewBuilder()->setLayout('productsLayout');
        $this->Authorization->skipAuthorization();
        $this->paginate = [
            'limit' => '4',
        ];
    }

    /**
     * Display Thumbnail view of all products
     *
     * @return void
     */
    public function index()
    {
        $this->set('products', $this->Products->allProduct());
    }

    /**
     * Buy products by productid
     *
     * @param int $productid Productid buy which buy a particular product
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function buy($productid)
    {
        $product = $this->Products->get($productid);
        if ($this->request->is('ajax')) {
            if ($product != "") {
                if ($product['quantity'] != 0) {
                    $new_qty = $product['quantity'] - 1;
                    $userid = $this->getRequest()->getSession()->read('Auth.id');
                    $usersproducts = $this->UsersProducts->newEmptyEntity();
                    $usersproducts->product_id = $productid;
                    $usersproducts->user_id = $userid;
                    if ($this->UsersProducts->save($usersproducts)) {
                        $product->quantity = $new_qty;
                        if ($this->Products->save($product)) {
                            $response = json_encode([
                                'status' => 'success', 'message' => 'Buy Product Successfully!',
                                'data' => ['alert-class' => 'alert-success'],
                            ]);
                            $this->response = $this->response->withStringBody($response);
                        }
                    }
                } else {
                    $response = json_encode([
                        'status' => 'fail', 'message' => 'Product Out of Stock!',
                        'data' => ['alert-class' => 'alert-danger'],
                    ]);
                    $this->response = $this->response->withStringBody($response);
                }
            } else {
                $response = json_encode([
                    'status' => 'fail', 'message' => 'Product Not Found!',
                    'data' => ['alert-class' => 'alert-danger'],
                ]);
                $this->response = $this->response->withStringBody($response);
            }
        }

        return $this->response;
    }

    /**
     * Table view of add to cart products of users
     *
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function cart()
    {
        try {
            $userid = $this->getRequest()->getSession()->read('Auth.id');
            $product = $this->paginate($this->UsersProducts->find('all', [
                'contain' => ['Products'],
                ])
            ->where(['user_id' => $userid]));
            $this->set('usersproducts', $product);
        } catch (NotFoundException $e) {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Product Remove from Cart by user
     *
     * @param int $id userproduct id used for remove product
     * @return \App\Controller\Cake\Http\Response|null
     */
    public function removeFromCart($id)
    {
        $userproduct = $this->UsersProducts->get($id);
        if ($this->request->is('ajax')) {
            $productid = $userproduct['product_id'];
            $product = $this->Products->get($productid);
            if ($product != "") {
                $new_qty = $product['quantity'] + 1;
                if ($this->UsersProducts->delete($userproduct)) {
                    $product->quantity = $new_qty;
                    if ($this->Products->save($product)) {
                        $response = json_encode([
                            'status' => 'success', 'message' => 'Product Remove Successfully!',
                            'data' => ['alert-class' => 'alert-success'],
                        ]);
                        $this->response = $this->response->withStringBody($response);
                    }
                }
            } else {
                 $response = json_encode([
                    'status' => 'fail', 'message' => 'Product Not Found!',
                    'data' => ['alert-class' => 'alert-danger'],
                 ]);
            }
        }

        return $this->response;
    }
}
