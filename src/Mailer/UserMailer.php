<?php
declare(strict_types=1);

namespace App\Mailer;

use Cake\Mailer\Mailer;

class UserMailer extends Mailer
{
    /**
     * Send mail to resetpassword of user
     *
     * @param array $emaildata it conatins the array of email and token
     * @return void
     */
    public function forgotpassword($emaildata)
    {
        $this
        ->setTo($emaildata['0'])
        ->setSubject(sprintf('Forgot Password Mail'))
        ->setViewVars(['user' => $emaildata['1']]);
    }

    /**
     * Send mail of verification of email
     *
     * @param array $emaildata it conatins the array of email and token
     * @return void
     */
    public function verification($emaildata)
    {
        $this
        ->setTo($emaildata['0'])
        ->setSubject(sprintf('Email Verification Mail'))
        ->setViewVars(['user' => $emaildata['1']]);
    }
}
