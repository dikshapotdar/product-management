<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Product extends Entity
{
    /**
     * Set upper case name before store  in database
     *
     * @param string $name Name of product
     * @return string
     */
    protected function _setName($name)
    {
        return ucwords($name);
    }
}
