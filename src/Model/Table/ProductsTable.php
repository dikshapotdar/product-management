<?php
declare(strict_types=1);

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table
{
    /**
     * Intialize method add associations, Behaviors which is used in the methods
     *
     * @param array $config gives array of objects
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsToMany('Users', [
            'joinTable' => 'users_products',
        ]);
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always',
                ],
            ],
        ]);
    }

    /**
     * This method sanitize the data which is provided by user through form
     *
     * @param \Cake\Event\Event $event instance of Cake\Event\Event class
     * @param \ArrayObject $data The data parameter is the array object instance
     * @param \ArrayObject $options provide options if needed
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data)) {
            foreach ($data as $key => $value) {
                if ($key != "image") {
                    $data[$key] = h($value);
                }
            }
        }
    }

    /**
     * This method validates the data provided by user
     *
     * @param \Cake\Validation\Validator $validator gives entities for validation
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->notEmptyString('name')
            ->notEmpty('price')
            ->notEmpty('quantity')
            ->notEmptyString('description');

        return $validator;
    }

    /**
     * Validation for Add method
     *
     * @param \Cake\Validation\Validator $validator gives entities for validation
     * @return \Cake\Validation\Validator
     */
    public function validationAdd(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->add('price', [
                'numeric' => [
                    'rule' => 'numeric',
                    'message' => 'Invalid Product Price!',
                ],
            ])
            ->notEmptyFile('image')
            ->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']],
                    'message' => __('Invalid file extension!'),
                ],
            ]);

        return $validator;
    }

    /**
     * Validation for Edit method
     *
     * @param \Cake\Validation\Validator $validator gives entities for validation
     * @return \Cake\Validation\Validator
     */
    public function validationEdit(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->add('price', [
                'numeric' => [
                    'rule' => 'numeric',
                    'message' => 'Invalid Product Price!',
                ],
            ])

            ->add('image', [
                'validExtension' => [
                    'rule' => ['extension',['gif', 'jpeg', 'png', 'jpg']],
                    'message' => __('Invalid file extension!'),
                ],
            ])
            ->allowEmptyFile('image');

        return $validator;
    }

    /**
     * Query for finding all products.
     *
     * @return object
     */
    public function allProduct()
    {
        $product = $this->find('all', [
            'contain' => ['Users'],
        ]);

        return $product;
    }
}
