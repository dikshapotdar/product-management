<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;

class UsersProductsTable extends Table
{
    /**
     * Intialize method add associations which is used in the methods
     *
     * @param array $config gives array of objects
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('Users');
        $this->belongsTo('Products');
    }
}
