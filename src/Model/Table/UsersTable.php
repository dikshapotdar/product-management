<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    /**
     * Intialize method add associations, Behaviors which is used in the methods
     *
     * @param array $config gives array of objects
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsToMany('Products', [
            'joinTable' => 'users_products',
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always',
                ],
            ],
        ]);
    }

    /**
     * BuildRules for email filed
     *
     * @param \Cake\ORM\RulesChecker $rules gives entity for Buildrules
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email'], 'This email is already exist!'));

        return $rules;
    }

    /**
     * Default validation
     *
     * @param \Cake\Validation\Validator $validator gives entities for validation
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->notEmptyString('name')
            ->notEmptyString('email')
            ->notEmptyString('password');

        return $validator;
    }

    /**
     * Validation for Register method
     *
     * @param \Cake\Validation\Validator $validator gives entities for validation
     * @return \Cake\Validation\Validator
     */
    public function validationRegister(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->add('name', [
                'onlyalphabet' => [
                    'rule' => ['custom', '/^[a-zA-Z]+ [a-zA-Z]+$/'],
                    'message' => 'Please Enter Valid Name!',
                ],
            ])
            ->add('email', [
                'validFormat' => [
                    'rule' => ['custom','/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/'],
                    'message' => 'Please Enter Valid Email!',
                ],
            ])

            ->add('password', [
                'validFormat' => [
                    'rule' => ['custom','/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/'],
                    'message' => 'Password must contain uppercase , lowercase , special character nd numerical!',
                ],
            ])

            ->add('confirmPassword', [
                'match' => [
                    'rule' => ['compareWith','password'],
                    'message' => 'The passwords does not match!',
                ],
            ]);

        return $validator;
    }

    /**
     * Validation for Resetpassword method
     *
     * @param \Cake\Validation\Validator $validator gives entities for validation
     * @return \Cake\Validation\Validator
     */
    public function validationResetpassword(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->add('newPassword', [
                'validFormat' => [
                    'rule' => ['custom','/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/'],
                    'message' => 'Password must contain uppercase , lowercase , special character nd numerical!',
                ],
            ])
            ->notEmpty('newPassword');

        $validator
            ->add('confirmPassword', [
                'match' => [
                    'rule' => ['compareWith','newPassword'],
                    'message' => 'The passwords does not match!',
                ],
            ])
            ->notEmpty('confirmPassword');

        return $validator;
    }

    /**
     * Query for finding the user contain that token.
     *
     * @param string $token user's uniuqe token
     * @return object
     */
    public function verifyToken($token)
    {
        $user = $this->find('all')->where(['token' => $token])->first();

        return $user;
    }
}
