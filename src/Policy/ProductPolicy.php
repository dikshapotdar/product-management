<?php
declare(strict_types=1);

namespace App\Policy;

use Authorization\IdentityInterface;

class ProductPolicy
{
    /**
     * CheckRole of login user
     *
     * @param \Authorization\IdentityInterface $user gives entities for validation
     * @return bool
     */
    public function checkRole(IdentityInterface $user)
    {
        if ($user->role == 'admin') {
            return true;
        }

        return false;
    }

    /**
     * CheckRole of login user
     *
     * @param \Authorization\IdentityInterface $user gives entities for validation
     * @return string
     */
    public function canIndex(IdentityInterface $user)
    {
        return $this->checkRole($user);
    }

    /**
     * CheckRole of login user
     *
     * @param \Authorization\IdentityInterface $user gives entities for validation
     * @return string
     */
    public function canAdd(IdentityInterface $user)
    {
        return $this->checkRole($user);
    }

    /**
     * CheckRole of login user
     *
     * @param \Authorization\IdentityInterface $user gives entities for validation
     * @return string
     */
    public function canEdit(IdentityInterface $user)
    {
        return $this->checkRole($user);
    }
}
