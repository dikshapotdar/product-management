<div class = 'row'>
	<div class = "col-md-6">
 		<h3>List of Buy Products</h3>
 	</div>	
</div>
<table class = "table table-bordered table-striped">
	<thead>
		<tr>
			<th><?= $this->Paginator->sort('name','Product Name') ?></th>
			<th><?= $this->Paginator->sort('name','User Name') ?></th>
			<th>Image</th>	
		</tr>
	</thead> 
	<tbody>
		<?php

		foreach($products as $key => $product):
			foreach($product->users as $key => $user):
		?>
		<tr>
			<td><?php echo $product['name'] ?></td>
			<td><?php echo $user['name'] ?></td>
			<td><?php
			$file ='image/'.$product['img'];
				echo $this->Html->image($file,['alt'=>'Image','style'=>'width:150px;border:1px #ccc solid'])?>
					
			</td>
		</tr>
		<?php
		endforeach;
		endforeach;
		?>
	</tbody>
</table>
<nav>
	<ul class = "pagination">
		<?php
		echo $this->Paginator->first();

		if($this->Paginator->hasPrev()){
			echo $this->Paginator->prev();
		}
		echo $this->Paginator->numbers();
		if($this->Paginator->hasNext()){
			echo $this->Paginator->next();
		}
		echo $this->Paginator->last();
		?>
	</ul>
</nav>		
