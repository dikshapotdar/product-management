<div class = "row">
	<div class = "col-md-6 offset-md-4">
		<div class = "card">
			<div class = "card-body">
				<?php echo $this->Form->create($product, ['type' => 'file']); ?>
				<br>
				<h3>Update Product</h3>
				<br>
				<div class = "from-group">
					<?php echo $this->Form->control('name',['label' => 'Product Name', 'required' =>false, 'class' => ($this->Form->isFieldError('name')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<div class = "from-group">
					<?php echo $this->Form->control('price',['required' => false ,'class' => ($this->Form->isFieldError('price')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<div class = "from-group">
					<?php echo $this->Form->control('quantity',['required' => false, 'class' => ($this->Form->isFieldError('quantity')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<div class = "from-group">
					<?php echo $this->Form->control('description',['required' => false, 'type' => 'textarea', 'class' => ($this->Form->isFieldError('description')) ? 'form-control is-invalid' : 'form-control'], ['escape' => false]); ?>
				</div>
				<br>
				<div class = "from-group">
					<?php echo $this->Form->control('image',['type' => 'file', 'required' => false, 'class' => ($this->Form->isFieldError('image')) ? 'is-invalid' : '']); ?>
				</div>
				<div>	
					<br>
					<?php
					$file = 'image/'.$product->img;
					
					echo $this->Html->image($file,['alt'=>'Image','style'=>'width:150px;border:1px #ccc solid']);
					?>
				</div>
				<br>
				<?php echo $this->Form->button('Update',['class'=>'btn btn-primary']) ?>
				<?php echo $this->Form->end() ?>
			</div>	
		</div>
	</div>
</div>		