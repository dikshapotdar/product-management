<div class='alert' id='flash' role="alert" onclick="this.classList.add('hidden');">
</div>
<div class="container">	
	<div class = 'row'>
		<div class = "col-md-6">
	 		<h3>List of Products</h3>
	 	</div>
	 	<div class = 'col-md-6 text-right'>
	 		<?php echo $this->Html->link('Add Data',['_name'=>'add'],['class'=>'btn btn-primary']); ?>
	 	</div>		
	</div>
	<table class = "table table-bordered table-striped">
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('name','Name') ?></th>
				<th><?= $this->Paginator->sort('price','Price') ?></th>
				<th><?= $this->Paginator->sort('quantity','Qty') ?></th>
				<th>Image</th>
				<th>Description</th>
				<th width="220">Action</th>	
			</tr>
		</thead> 
		<tbody>
			<?php
			foreach($products as $product):
			?>
			<tr>
				<td><?php echo $product->name ?></td>
				<td><?php echo $product->price ?></td>
				<td><?php echo $product->quantity ?></td>
				<td><?php
				$file ='image/'.$product->img;
					echo $this->Html->image($file,['alt'=>'Image','style'=>'width:150px;border:1px #ccc solid'])?>
						
				</td>
				<td><?php echo $product->description ?></td>
				<td>
					<?php echo $this->Html->link('Edit',['_name'=>'edit',$product->id],['class'=>'btn btn-warning']); ?>
					<input type = "button" class = "btn btn-danger btn-delete" value = "Delete" data_url="<?= $this->Url->build(['_name' => 'delete']) ?>" product_id = "<?php echo $product->id;?>"/>
				</td>
			</tr>
			<?php
			endforeach;
			?>
		</tbody>
	</table>
	<nav>
		<ul class = "pagination">
			<?php
			echo $this->Paginator->first();

			if($this->Paginator->hasPrev()){
				echo $this->Paginator->prev();
			}
			echo $this->Paginator->numbers();
			if($this->Paginator->hasNext()){
				echo $this->Paginator->next();
			}
			echo $this->Paginator->last();
			?>
		</ul>
	</nav>	
</div>		