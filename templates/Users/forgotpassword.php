<div class = "row">
	<div class = "col-md-6 offset-md-4">
		<?php 
		$myTemplates = [
			'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
			'error' => '<div class="invalid-feedback">{{content}}</div>'

		];
		$this->Form->setTemplates($myTemplates);
		?>
		<div class = "card">
			<div class = "card-body">
				<?php echo $this->Form->create(); ?>
				<br>
				<h3>Forgot Password</h3>
				<br>
				<div class = "from-group">
					<?php echo $this->Form->control('email',['type'=> 'text' ,'required' =>false ,'class' => 'form-control']); ?>
				</div>
				<br>
				<button type = "submit" class = "btn btn-success">Submit</button>
				<?php echo $this->Html->link('Back', ['_name' => 'login'],['class'=>'btn btn-primary']); ?>
				<?php echo $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>		