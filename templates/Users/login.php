<div class = "row">
	<div class = "col-md-6 offset-md-4">
		<?php 
		$myTemplates = [
			'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
			'error' => '<div class="invalid-feedback">{{content}}</div>'

		];
		$this->Form->setTemplates($myTemplates);
		?>
		<div class = "card">
			<div class = "card-body">
				<?php echo $this->Form->create(); ?>
				<br>
				<h3>Login</h3>
				<br>
				<div class = "from-group">
					<?php echo $this->Form->control(
						'email',
						[
							'type' => 'text' ,
							'required' => false ,
							'class' => ($this->Form->isFieldError('email')) ? 'form-control is-invalid' : 'form-control'
						]); 
					?>
				</div>
				<div class = "from-group">
					<?php echo $this->Form->control('password',['required' =>false ,'class' => ($this->Form->isFieldError('password')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<br>
				<button type = "submit" class = "btn btn-success">Login</button>
				<?php echo $this->Html->link('Register',['_name' => "register"],['class' => 'btn btn-primary']); ?>
				<?php echo $this->Form->end() ?>
			</div>

		</div>
		<div align="center">
		<?php echo $this->Html->link("Forgot Password!", ['_name' => 'forgotpassword']);?>
		</div>
	</div>
</div>		