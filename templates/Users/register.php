<div class = "row">
	<div class = "col-md-6 offset-md-4">
		<?php 
		$myTemplates = [
			'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
			'error' => '<div class="invalid-feedback">{{content}}</div>'

		];
		$this->Form->setTemplates($myTemplates);
		?>
		<div class = "card">
			<div class = "card-body">
				<?php echo $this->Form->create($users); ?>
				<br>
				<h3>Registration</h3>
				<br>
				<div class = "from-group">
					<?php echo $this->Form->control('name',['label'=>'Full Name','required' =>false ,'class' => ($this->Form->isFieldError('name')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<div class = "from-group">
					<?php echo $this->Form->control('email',['required' =>false ,'type' => 'text','class' => ($this->Form->isFieldError('email')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<div class = "from-group">
					<?php echo $this->Form->control('password',['required' =>false ,'class' => ($this->Form->isFieldError('password')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<div class = "from-group">
				<?php echo $this->Form->control('confirmPassword',['label'=>'Confirm Password','type'=>'password','required' =>false ,'class' => ($this->Form->isFieldError('confirmPassword')) ? 'form-control is-invalid' : 'form-control']); ?>
				</div>
				<br>
				<button type = "submit" class = "btn btn-primary">Register</button>
				<?php echo $this->Html->link('Back',['action'=>"login"],['class'=>'btn btn-success']); ?>
				<?php echo $this->Form->end() ?>
			</div>	
		</div>
	</div>
</div>		