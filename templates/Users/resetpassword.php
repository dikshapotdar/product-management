<div class = "row">
<div class = "col-md-6 offset-md-4">
	<?php 
	$myTemplates = [
		'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
		'error' => '<div class="invalid-feedback">{{content}}</div>'

	];
	$this->Form->setTemplates($myTemplates);
	?>
	<div class = "card">
		<div class = "card-body">
			<?php echo $this->Form->create($user);?>
			<br>
			<h3>Reset Password</h3>
			<br>
			<div class = "from-group">
				<?php echo $this->Form->control('newPassword',['label'=>'New Password','type'=>'password','required' =>false ,'class' => ($this->Form->isFieldError('newPassword')) ? 'form-control is-invalid' : 'form-control']); ?>
			</div>
			<br>
			<div class = "from-group">
				<?php echo $this->Form->control('confirmPassword',['label'=>'Confirm Password','type'=>'password','required' =>false ,'class' => ($this->Form->isFieldError('confirmPassword')) ? 'form-control is-invalid' : 'form-control']); ?>
			</div>
			<br>
			<?php echo $this->Form->button('Reset Password',['class'=>'btn btn-primary']) ?>
			<?php echo $this->Form->end() ?>
		</div>	
	</div>
</div>
</div>