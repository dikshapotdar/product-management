<div class='alert' id='flash' role="alert" onclick="this.classList.add('hidden');">
</div>
<div class="container">
	<div class = 'row'>
		<div class = "col-md-6">
	 		<h3>List of Buy Products</h3>
	 	</div>	
	</div>
	<table class = "table table-bordered table-striped" >
		<thead>
			<tr>
				<th><?= $this->Paginator->sort('name','Product Name') ?></th>
				<th>Image</th>
				<th><?= $this->Paginator->sort('date','Date/Time') ?></th>
				<th>Action</th>	
			</tr>
		</thead> 
		<tbody>
			<?php
			foreach($usersproducts as $key => $userproduct):
			?>
			<tr>
				<td><?php echo $userproduct->product['name'] ?></td>
				<td><?php
				$file ='image/'.$userproduct->product['img'];
					echo $this->Html->image($file,['alt'=>'Image','style'=>'width:150px;border:1px #ccc solid'])?>
						
				</td>
				<td><?php echo $userproduct['date'] ?></td>
				<td>
					<input type = "button" class = "btn btn-danger btn-remove" value = "Delete" data_url="<?= $this->Url->build(['_name' => 'removeFromCart']) ?>" id = "<?php echo $userproduct->id;?>"/>
				</td>
			</tr>
			<?php
			endforeach;
			?>
		</tbody>
	</table>
	<nav>
		<ul class = "pagination">
			<?php
			echo $this->Paginator->first();

			if($this->Paginator->hasPrev()){
				echo $this->Paginator->prev();
			}
			echo $this->Paginator->numbers();
			if($this->Paginator->hasNext()){
				echo $this->Paginator->next();
			}
			echo $this->Paginator->last();
			?>
		</ul>
	</nav>
</div>	

