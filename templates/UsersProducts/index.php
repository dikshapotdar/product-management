<div class='alert' id='flash' role="alert" onclick="this.classList.add('hidden');">
</div>
<div class = 'row'>
    <div class = "col-md-6">
        <h3>Products</h3>
    </div>
</div>     
<div class = 'col-md-10 text-right'>
    <?php echo $this->Html->link('My Cart',['_name'=>'cart'],['class'=>'btn btn-danger']); ?>
</div>
<div class="row">
    <?php foreach ($products as $product):?>
    <div class="col-sm-6 col-md-4">
        <div class="">
            <br>
            <?php
            $file ='image/'.$product->img;
                echo $this->Html->image($file,['alt'=>'Image','style'=>'width:150px;border:1px #ccc solid'],
                    ['escape'=>false,'class'=>'thumbnail']);?>
            <div class="caption">
                <h5>
                    <?php echo $product->name;?>
                </h5>
                <h5>
                    Price: $
                    <?php echo $product->price;?>
                </h5>
                <input type = "button" class = "btn btn-primary btn-buy" value = "Add To Cart" data_url="<?= $this->Url->build(['_name' => 'buy']) ?>" id = "<?php echo $product->id;?>"/>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>


