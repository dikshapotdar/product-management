<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert" role="alert" onclick="this.classList.add('hidden');">
 <h3><?= $message ?></h3>
</div>