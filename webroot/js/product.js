$("#flash").hide();
$(document).ready(function() {
    $(".btn-buy").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("data_url");
        var id = $(this).attr("id");
        var csrfToken = $('meta[name="csrfToken"]').attr('content');
        $(".alert").removeClass("alert-success");
        $(".alert").removeClass("alert-danger");
        $.ajax({
            "url": url + "/" + id,
            "type": "post",
            "headers": {
                'X-CSRF-Token': csrfToken
            },
            success: function(response) {
                var response = JSON.parse(response);
                $(".alert").addClass(response.data['alert-class']);
                $("#flash").show();
                $("#flash").html(response.message);
            },
            error: function(response) {
                console.log('error');
            }
        });
    });

    $(".btn-remove").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("data_url");
        var id = $(this).attr("id");
        var csrfToken = $('meta[name="csrfToken"]').attr('content');
        $(".alert").removeClass("alert-success");
        $(".alert").removeClass("alert-danger");
        bootbox.confirm("Are you sure you want to delete this product ?", function(result){
            if (result) {
                $.ajax({
                    "url": url + "/" + id,
                    "type": "post",
                    "headers": {
                        'X-CSRF-Token': csrfToken
                    },
                    success: function(response) {
                        var response = JSON.parse(response);
                        $(".container").load(window.location.href + " .container >*");
                        $(".alert").addClass(response.data['alert-class']);
                        $("#flash").show();
                        $("#flash").html(response.message);
                    },
                    error: function(response) {
                       console.log('error');
                    }
                });
            }
        })
    }); 

    $(".btn-delete").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("data_url");
        var id = $(this).attr("product_id");
        var csrfToken = $('meta[name="csrfToken"]').attr('content');
        $(".alert").removeClass("alert-success");
        $(".alert").removeClass("alert-danger");
        bootbox.confirm("Are you sure you want to delete this product ?", function(result){
            if (result) {
                $.ajax({
                    "url": url + "/" + id,
                    "type": "post",
                    "headers": {
                        'X-CSRF-Token': csrfToken
                    },
                    success: function(response) {
                        var response = JSON.parse(response);
                        $(".container").load(window.location.href + " .container >*");
                        $(".alert").addClass(response.data['alert-class']);
                        $("#flash").show();
                        $("#flash").html(response.message);
                    },
                    error: function(response) {
                        console.log('error');
                    }
                });
            }
        })  
    });   
});